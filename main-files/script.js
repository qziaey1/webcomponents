function toggleMapModes() {
    var getMapMode = document.getElementById("map-styles");
    if (getMapMode.style.display == "none") {
        getMapMode.style.display = "block";
    } else {
        getMapMode.style.display = "none";
    }
}


// This fuction is to toggle aside menu

function toggleAsideMenu() {
    var getAsideMenu = document.getElementsById("aside");
    if (getAsideMenu.style.display == "none") {
        getAsideMenu.style.display = "block";
    } else {
        getAsideMenu.style.display = "none";
    }
}